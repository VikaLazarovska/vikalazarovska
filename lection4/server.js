const express = require('express');
const postsRouter = require("./controllers/posts.controller");
const es6Renderer = require("express-es6-template-engine");
const fetch = require('node-fetch');
const app = express();
app.engine("html", es6Renderer);
app.set("views", __dirname + "/views");
app.use(express.static("public"));
app.use(express.json());

app.use("/posts", postsRouter);

app.listen(3000, () => {
    console.log("Server is up!");
})


