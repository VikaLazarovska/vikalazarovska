const express = require("express");
const router = express.Router();
const fetch = require('node-fetch');

    router.get('/stuff', (req, res) => {
        res.status(404).render('index.html');

    })

    router.get('/stuff/page', (req, res) => {
        res.send('This is stuff page!');
    })

    router.get('/stuff/data', (req, res) => {
        res.json({data: "This is stuff data"});
    })

    router.post('/stuff/set', (req, res) => {
        if (Object.keys(req.body).length === 0) {
            res.status(404);
            res.json({message: "No body passed!"});
        } else {
            res.json({message: "OK"});
        }
    })

    router.get('/', (req, res) => {
        res.status(404).render('index.html');
    })



    router.get('/filtered', async (req, res) => {
        let posts = await fetch("https://jsonplaceholder.typicode.com/posts")
            .then(resp => resp.json());
            let i = 0;
            let someString = req.query.filter;
            let users = [];
            do {
                if (posts[i].title.includes(someString)) {
                    users.push(posts[i]);
                }
                i++;

            } while (i < posts.length);

            if (users.length === 0) {
                users.push('Not found')
            }

            res.send(users);

    })

module.exports = router;